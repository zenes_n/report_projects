import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.sql.Timestamp;

@Entity
@Table(name = "specialdate")
public class Holidays {
    @Id
    private int id;
    private String country;
    private Timestamp data;
    private int hours;
    private String name;
    private int type;
    private int version;

    public int getId() {
        return id;
    }

    public String getCountry() {
        return country;
    }

    public Timestamp getData() {
        return data;
    }

    public int getHours() {
        return hours;
    }

    public String getName() {
        return name;
    }

    public int getType() {
        return type;
    }

    public int getVersion() {
        return version;
    }

    @Override
    public String toString() {
        return "Holidays{" +
                "id=" + id +
                ", country='" + country + '\'' +
                ", data=" + data +
                ", hours=" + hours +
                ", name='" + name + '\'' +
                ", type=" + type +
                ", version=" + version +
                '}';
    }
}
