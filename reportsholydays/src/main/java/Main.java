import net.sf.dynamicreports.report.builder.DynamicReports;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRMapCollectionDataSource;
import net.sf.jasperreports.view.JasperViewer;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.util.*;


public class Main {

    final static String HQL_HOLIDAYS =  " FROM Holidays\n" +
                                         "WHERE to_char(data, 'YYYY') = 2017\n";

    final static String HQL_CROSSTAB =  "SELECT country, TO_CHAR(data, 'MM') AS mnth, COUNT(name) as holidays\n" +
                                        "  FROM Holidays\n" +
                                        " WHERE TO_CHAR(data, 'YYYY') = 2017\n" +
                                        " GROUP BY country, TO_CHAR(data, 'MM')\n" +
                                        " ORDER BY mnth";

    final static String HQL_BARCHART =  "SELECT TO_CHAR(DATA, 'MM') AS mnth," +
                                        "       SUM(CASE WHEN country = 'MD' THEN 1 ELSE 0 END) AS nr_md," +
                                        "       SUM(CASE WHEN country = 'IT' THEN 1 ELSE 0 END) AS nr_it" +
                                        "  FROM Holidays" +
                                        " WHERE TO_CHAR(data, 'YYYY') = 2017" +
                                        " GROUP BY TO_CHAR(DATA, 'MM')" +
                                        " ORDER BY mnth";

    public static void main(String[] args) {

        Main m = new Main();
        m.start();
    }

    public void start() {
        try {
            /*
             * Initializing factory
             * */
            SessionFactory factory = new Configuration()
                    .configure("hibernate.cfg.xml")
                    .addAnnotatedClass(Holidays.class)
                    .buildSessionFactory();



            /*
             * Preparing the collections for records
             * */
            List<Holidays> records = new ArrayList<Holidays>();
            List<Object[]> crosstabList;
            List<Object[]> barchartList;
            Collection<Map<String, ?>> holidaysList = new ArrayList<Map<String, ?>>();
            Map<String, Object> holidaysMap;

            /*
             * Preparing the session
             * */
            Session session = factory.getCurrentSession();
            try {
                session.beginTransaction();

                /*
                 * Retreving the records
                 * */
                records = session.createQuery(HQL_HOLIDAYS).list();
                crosstabList = session.createQuery(HQL_CROSSTAB).list();
                barchartList = session.createQuery(HQL_BARCHART).list();

                /*
                 * Filling the holidaysList with maps of records
                 * */
                for (Holidays a : records) {
                    holidaysMap = new HashMap<String, Object>();

                    holidaysMap.put("country", a.getCountry());
                    holidaysMap.put("name", a.getName());
                    holidaysMap.put("data", a.getData());

                    holidaysList.add(holidaysMap);
                }
            } finally {
                session.close();
            }

            /*
             * Jasper report
             * */
            try{
                jasperReport("src/main/resources/holidays_report.jasper", holidaysList);
            } catch (Exception e){
                e.printStackTrace();
            }

            /*
             * Simple report
             * */
            SimpleReport simpleReport = new SimpleReport(records);

            /*
             * Crosstab report
             * */
            CrosstabReport crosstabReport = new CrosstabReport(crosstabList, barchartList);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void jasperReport(String stream, Collection listOfObjects) throws Exception {

        /*
         * Reading the template
         * */
        FileInputStream report = new FileInputStream(stream);
        BufferedInputStream bufferedInputStream = new BufferedInputStream(report);
        JRMapCollectionDataSource dataSource = new JRMapCollectionDataSource(listOfObjects);

        /*
         * Compiling report
         * */
        JasperPrint jasperPrint = JasperFillManager.fillReport(report, new HashMap(), dataSource);
        JasperViewer.viewReport(jasperPrint, false);
    }

    public void startDynamicReport() {
        try {
            DynamicReports dynamicReports = new DynamicReports();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}