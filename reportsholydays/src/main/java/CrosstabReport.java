import net.sf.dynamicreports.examples.Templates;
import net.sf.dynamicreports.report.builder.column.TextColumnBuilder;
import net.sf.dynamicreports.report.builder.crosstab.CrosstabBuilder;
import net.sf.dynamicreports.report.builder.crosstab.CrosstabColumnGroupBuilder;
import net.sf.dynamicreports.report.builder.crosstab.CrosstabRowGroupBuilder;
import net.sf.dynamicreports.report.builder.style.FontBuilder;
import net.sf.dynamicreports.report.constant.Calculation;
import net.sf.dynamicreports.report.constant.PageOrientation;
import net.sf.dynamicreports.report.constant.PageType;
import net.sf.dynamicreports.report.datasource.DRDataSource;
import net.sf.dynamicreports.report.exception.DRException;
import net.sf.jasperreports.engine.JRDataSource;

import java.util.Date;
import java.util.List;

import static net.sf.dynamicreports.report.builder.DynamicReports.*;

public class CrosstabReport {
    private static List<Object[]> holidaysList;
    private static List<Object[]> barChartList;

    public CrosstabReport(List<Object[]> holidaysList, List<Object[]> barChartList) {
        this.holidaysList = holidaysList;
        this.barChartList = barChartList;
        build();
    }

    private void build() {
        /*
        * Crosstab
        * */
        CrosstabRowGroupBuilder<String> rowGroup = ctab.rowGroup("state", String.class)
                .setTotalHeader("Total holidays");
        CrosstabColumnGroupBuilder<String> columnGroup = ctab.columnGroup("month", String.class);
        CrosstabBuilder crosstab = ctab.crosstab()
                .headerCell(cmp.text("State / Mese").setStyle(Templates.boldCenteredStyle))
                .rowGroups(rowGroup)
                .columnGroups(columnGroup)
                .measures(
                        ctab.measure("Month", "nrOfHolidays", Long.class, Calculation.SUM)
                );
        try {
            report()
                    .setPageFormat(PageType.A4, PageOrientation.LANDSCAPE)
                    .setTemplate(Templates.reportTemplate)
                    .title(Templates.createTitleComponent("Crosstab"))
                    .summary(crosstab)
                    .pageFooter(Templates.footerComponent)
                    .setDataSource(createDataSource())
                    .show();


            FontBuilder boldFont = stl.fontArialBold().setFontSize(12);

            TextColumnBuilder<String> itemColumn = col.column("Month", "month", type.stringType());
            TextColumnBuilder<Integer> holidaysMd = col.column("MD", "holidaysMd", type.integerType());
            TextColumnBuilder<Integer> holidaysIt = col.column("IT", "holidaysIt", type.integerType());

            report().setTemplate(Templates.reportTemplate)
                    .columns(itemColumn, holidaysMd, holidaysIt)
                    .title(Templates.createTitleComponent("BarChart"))

                    .summary(
                            cht.barChart()
                                    .setTitle("Holidays")
                                    .setTitleFont(boldFont)
                                    .setCategory(itemColumn)
                    .series(
                            cht.serie(holidaysMd),
                            cht.serie(holidaysIt)
                    )
                    .setCategoryAxisFormat(cht.axisFormat().setLabel("Total")))
                    .pageFooter(Templates.footerComponent).setDataSource(createBarchartSource())
                    .show();

        } catch (DRException e) {
            e.printStackTrace();
        }
    }

    private void buildChart() {

    }

    private JRDataSource createDataSource() {
        DRDataSource dataSource = new DRDataSource("state", "month", "nrOfHolidays");
        for (Object[] record : holidaysList) {
            dataSource.add(record[0], record[1], record[2]);
        }
        return dataSource;
    }

    private JRDataSource createBarchartSource(){

        DRDataSource dataSource = new DRDataSource("month", "holidaysMd", "holidaysIt");
        for(Object[] record: barChartList){
            dataSource.add(record[0].toString(), Integer.parseInt(record[1].toString()), Integer.parseInt(record[2].toString()));
        }

        return dataSource;
    }
}
