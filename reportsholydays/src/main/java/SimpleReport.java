import net.sf.dynamicreports.examples.Templates;
import net.sf.dynamicreports.report.builder.style.FontBuilder;
import net.sf.dynamicreports.report.builder.style.StyleBuilder;
import net.sf.dynamicreports.report.constant.HorizontalTextAlignment;
import net.sf.dynamicreports.report.constant.VerticalTextAlignment;
import net.sf.dynamicreports.report.datasource.DRDataSource;
import net.sf.dynamicreports.report.exception.DRException;
import net.sf.jasperreports.engine.JRDataSource;

import java.awt.*;
import java.util.List;

import static net.sf.dynamicreports.report.builder.DynamicReports.*;

public class SimpleReport {
    private List<Holidays> holidaysList;

    public SimpleReport(List<Holidays> holidaysList) {
        this.holidaysList = holidaysList;
        build();
    }

    private void build() {


        StyleBuilder titleStyle = stl.style()
                                            .bold()
                                            .setHorizontalTextAlignment(HorizontalTextAlignment.CENTER);



        try {
            report()
                    .columns(
                            col.column("Country", "country", type.stringType()).setWidth(10),
                            col.column("Name", "name", type.stringType()).setWidth(60),
                            col.column("Date", "date", type.dateType()).setWidth(30)
                                    .setHorizontalTextAlignment(HorizontalTextAlignment.RIGHT)
                    )
                    .title(Templates.createTitleComponent("Holidays")) // Title
                    .setTitleStyle(titleStyle)
                    .pageFooter(cmp.pageXofY()) // Number of page
                    .setDataSource(createDataSource()) // Datasource
                    .show(); // Creating and showing the report
        } catch (DRException e) {
            e.printStackTrace();
        }
    }

    private JRDataSource createDataSource() {
        DRDataSource dataSource = new DRDataSource("country", "name", "date");

        for (Holidays item : this.holidaysList) {
            dataSource.add(item.getCountry(), item.getName(), item.getData());
        }
        return dataSource;
    }
}
